import scala.io.StdIn.readLine
object cipher extends App{
  val alph = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

  val encript = (c:Char,k:Int,a:String) => a((a.indexOf(c.toUpper)+k)%a.size);

  val decript = (c:Char,k:Int,a:String) => a((a.indexOf(c.toUpper)-k)%a.size);

  val cipher = (algo:(Char,Int,String)=>Char,s:String,k:Int,a:String)=>s.map(algo(_,k,a));

  printf("Enter a String : ");
  var str = readLine();
  var en = cipher(encript,str,1,alph);
  println("Encripted string : " + en);
  var dec = cipher(decript,en,1,alph);
  println("Decripted string : " + dec);
  
}

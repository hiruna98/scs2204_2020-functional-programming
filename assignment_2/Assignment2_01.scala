import scala.io.StdIn.readInt
object Assignment2_01 extends App{

  def normal(h:Int):Int = h*150;
  def ot(h:Int):Int = h*75;
  def salary(h1:Int,h2:Int):Int = normal(h1)+ot(h2);
  def tax(h1:Int,h2:Int):Double = salary(h1,h2)/10;
  def homesal(h1:Int,h2:Int):Double = salary(h1,h2)-tax(h1,h2);
  printf("Enter nomal hours : ");
  var h1 = readInt()
  printf("Enter ot hours : ");
  var h2 = readInt()
  var sal = homesal(h1,h2);
  print("Home salary = ")
  print(sal)

}

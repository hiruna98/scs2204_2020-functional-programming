import scala.math
object pointrep extends App
{
  var p1 = new Point(2,3)
  var p2 = new Point(4,5)
  println(p1)
  println(p2)
  println(p1.distance(p2))
  println(p1.move(3,3))
  println(p1+p2)
  println(p1)
  println(p1.invert)
}
case class Point(x:Int,y:Int){
  def move(p:Int,q:Int)= Point(this.x+p,this.y+q)
  def +(m:Point)= Point(this.x+m.x,this.y+m.y)
  def distance(n:Point):Double ={
    return math.sqrt(((this.x-n.x)*(this.x-n.x))+((this.y-n.y)*(this.y-n.y)))
  }
  def invert()= Point(this.y,this.x)
}

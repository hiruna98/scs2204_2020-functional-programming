import scala.io.StdIn.readInt
object Assignment3_01 extends App{

  def gcd(a:Int,b:Int):Int = b match{
    case 0 => a
    case x if x>a => gcd(x,a)
    case _ => gcd(b,a%b)

  }

  def isprime(a:Int,b:Int=2):Boolean = b match{
    case x if x==a => true
    case x if gcd(a,x)>1 => false
    case _ => isprime(a,b+1)
  }
  
  printf("Enter a number : ")
  var a = readInt()
  println("Is prime : " + isprime(a))

}

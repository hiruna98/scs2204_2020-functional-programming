import scala.io.StdIn.readInt
object Assignment3_06 extends App{

  def fibonacci(n:Int):Int = n match{
    case 0 => 0
    case 1 => 1
    case _ => fibonacci(n-1) + fibonacci(n-2)
  }
  def fibonacciSeq(n:Int):Unit = n match{
    case 0 => println(n)
    case _ => {
      fibonacciSeq(n-1)
      println(fibonacci(n))
    }
  }
  printf("Enter a Number : ")
  var a = readInt()
  fibonacciSeq(a)
}

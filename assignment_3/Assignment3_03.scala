import scala.io.StdIn.readInt
object Assignment3_03 extends App{

  def add(n:Int):Int = n match{
    case 0 => 0
    case _ => n + add(n-1)
  }
  printf("Enter a number : ")
  var a = readInt()
  println("Sum = " + add(a))

}

import scala.io.StdIn.readInt
object Assignment3_02 extends App{
  
  def gcd(n:Int,m:Int):Int = m match{
    case 0 => n
    case x if x>n => gcd(x,n)
    case _ => gcd(m,n%m)
  }
  def isprime(a:Int,b:Int=2):Boolean = b match{
    case x if x==a => true
    case x if gcd(x,a)>1 => false
    case _ => isprime(a,b+1)
  }
  def primeSeq(n:Int):Unit = n match{
    case 0 => println(n)
    case 2 => println(n)
    case _ => {
      primeSeq(n-1)
      if(isprime(n)){
        println(n)
      }
    }

  }
  printf("Enter a number : ")
  var a = readInt()
  print("Prime numbers : ")
  primeSeq(a)
}

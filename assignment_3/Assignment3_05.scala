import scala.io.StdIn.readInt
object Assignment3_05 extends App{
  
  def iseven(n:Int):Boolean = n match{
    case 0 => true
    case _ => isodd(n-1)
  }
  def isodd(m:Int):Boolean = !iseven(m);
  def sum(a:Int):Int = a match{
    case 0 => a
    case x if(iseven(x)) => x+sum(x-1)
    case x if(!iseven(x)) => sum(x-1)
  }

  printf("Enter a Number : ")
  var a = readInt()
  println("Sum : " + sum(a))
}

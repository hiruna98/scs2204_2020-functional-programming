import scala.io.StdIn.readInt
object Assignment3_04 extends App{

  def iseven(n:Int):Boolean = n match{
    case 0 => true
    case _ => isodd(n-1)
  }
  def isodd(m:Int):Boolean = !iseven(m);
  
  printf("Enter a Number : ")
  var a = readInt()
  println("Is even : " + iseven(a))
}

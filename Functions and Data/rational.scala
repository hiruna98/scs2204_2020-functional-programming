object rationalnumbers extends App
{
    val a = new Rational(2,3)
    val b = new Rational(4,8)
    val c = new Rational(1,8)
    println(a)
    println(b)
    println(a.sub(b,c))
}

class Rational(x:Int,y:Int)
{
    private val g = gcd(x,y)
    def num = x/g
    def dnum = y/g
    private def gcd(p:Int,q:Int):Int = q match{
      case 0 => p
      case _ => gcd(q,p%q)
    }
    def sub(m:Rational,n:Rational)= new Rational((this.num * m.dnum * n.dnum) - (m.num*this.dnum*n.dnum) - (n.num*m.dnum*this.dnum), this.dnum*m.dnum*n.dnum )
    def neg = new Rational(-this.num , this.dnum)
    override def toString = num + "/" + dnum
}

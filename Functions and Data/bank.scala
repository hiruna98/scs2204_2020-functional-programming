object bank extends App
{
  var a1 = new Account("v982860091","a100001",24)
  var a2 = new Account("v982867491","a100002",-54)
  var a3 = new Account("v986960091","a100003",-28)
  //println(a1)
  //println(a2)  
  //println(a3) 
  val l:List[Account] = List(a1,a2,a3)
  println("List of Accounts")
  println(l)
  println("List of Accounts with negative balances")
  println(l.filter(x=>x.bal < 0))
  println("Sum of all account balances")
  println(l.reduce((x,y)=>x.bal+y.bal))

}

class Account(d:String, b:String , c:Double){
  var nic:String = d;
  var acc:String = b
  var bal:Double = c

  def withdraw(a:Double) = {
    this.bal = this.bal -a
  }
  def deposit(a:Double) = {
    this.bal = this.bal + a
  }
  def transfer(x:Account,a:Double) = {
    this.bal = this.bal - a
    x.bal = x.bal + a
  }
  override def toString = "[" + nic + ":" + acc + ":" + bal + "]"
}
